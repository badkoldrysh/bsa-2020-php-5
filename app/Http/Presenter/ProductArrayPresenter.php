<?php

declare(strict_types=1);

namespace App\Http\Presenter;

use App\Entity\Product;

class ProductArrayPresenter
{
    /**
     * @param Product[] $products
     * @return array
     */
    public static function presentCollection(array $products): array
    {
        $productsCollection = [];

        foreach ($products as $product) {
            $productsCollection[] = self::present($product);
        }

        return $productsCollection;
    }

    /**
     * @param Product $product
     * @return array
     */
    public static function present(Product $product): array
    {
        return [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'price' => $product->getPrice(),
            'img' => $product->getImageUrl(),
            'rating' => $product->getRating(),
        ];
    }
}
