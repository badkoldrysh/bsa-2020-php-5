<?php

declare(strict_types=1);

namespace App\Action\Product;

class GetAllProductsAction
{
    private $response;

    public function __construct()
    {
        $this->response = app(GetAllProductsResponse::class);
    }

    /**
     * @return GetAllProductsResponse
     */
    public function execute(): GetAllProductsResponse
    {
        return $this->response;
    }
}
