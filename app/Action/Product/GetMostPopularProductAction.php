<?php

declare(strict_types=1);

namespace App\Action\Product;

class GetMostPopularProductAction
{
    private $response;

    public function __construct()
    {
        $this->response = app(GetMostPopularProductResponse::class);
    }

    /**
     * @return GetMostPopularProductResponse
     */
    public function execute(): GetMostPopularProductResponse
    {
        return $this->response;
    }
}
