<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;

class GetMostPopularProductResponse
{
    private $repository;

    /**
     * @param ProductRepositoryInterface $repository
     */
    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        $products = $this->repository->findAll();

        $maxRating = 0;
        $maxRatingProduct = null;
        foreach ($products as $product) {
            if ($product->getRating() > $maxRating) {
               $maxRatingProduct = $product;
            }
        }

        return $maxRatingProduct;
    }
}
