<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;

class GetCheapestProductsResponse
{
    private $repository;

    /**
     * @param ProductRepositoryInterface $repository
     */
    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return Product[]
     */
    public function getProducts(): array
    {
        $products = $this->repository->findAll();
        usort($products, function($a, $b) {
            return $a->getPrice() <=> $b->getPrice();
        });

        return array_slice($products, 0, 3);
    }
}
