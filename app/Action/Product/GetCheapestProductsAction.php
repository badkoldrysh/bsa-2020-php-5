<?php

declare(strict_types=1);

namespace App\Action\Product;

class GetCheapestProductsAction
{
    private $response;

    public function __construct()
    {
        $this->response = app(GetCheapestProductsResponse::class);
    }

    /**
     * @return GetCheapestProductsResponse
     */
    public function execute(): GetCheapestProductsResponse
    {
        return $this->response;
    }
}
