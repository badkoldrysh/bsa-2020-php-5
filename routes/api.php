<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/products', function() {
    $products = app(\App\Action\Product\GetAllProductsAction::class)->execute()->getProducts();

    $products_array = \App\Http\Presenter\ProductArrayPresenter::presentCollection($products);

    $json = json_encode($products_array);

    return Response($json,200)->header('Content-Type', 'application/json');
});

Route::get('/products/popular', function() {
   $product = app(\App\Action\Product\GetMostPopularProductResponse::class)->getProduct();

   $product_array = \App\Http\Presenter\ProductArrayPresenter::present($product);

   $json = json_encode($product_array);

   return Response($json, 200)->header('Content-Type', 'application/json');
});
