<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cheap products</title>
</head>
<body>
    <h1>Three most cheapest products ever</h1>
    <ol>
        @foreach ($products as $product)
            <li>
                <h4>{{ $product['name'] }}</h4>
                <img src="{{ $product['img'] }}" alt="{{ $product['name'] }}_image"/>
                <br />
                <p>Price: {{ $product['price'] }}</p>
                <p>Rating: {{ $product['rating'] }}</p>
            </li>
        @endforeach
    </ol>
</body>
</html>
